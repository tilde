#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <jack/jack.h>

static jack_port_t *input_port[2];
static jack_port_t *output_port[2];
static jack_client_t *client;

static int process(jack_nframes_t nframes, void *arg) {
  state *s = (state *) arg;
  jack_default_audio_sample_t *in[2], *out[2];
  in [0] = (jack_default_audio_sample_t *)jack_port_get_buffer(input_port [0], nframes);
  in [1] = (jack_default_audio_sample_t *)jack_port_get_buffer(input_port [1], nframes);
  out[0] = (jack_default_audio_sample_t *)jack_port_get_buffer(output_port[0], nframes);
  out[1] = (jack_default_audio_sample_t *)jack_port_get_buffer(output_port[1], nframes);
  for (jack_nframes_t i = 0; i < nframes; ++i) {
    s->adc[0] = in[0][i];
    s->adc[1] = in[1][i];
    s->dac[0] = 0;
    s->dac[1] = 0;
    dsp(s);
    out[0][i] = s->dac[0].x;
    out[1][i] = s->dac[1].x;
  }
  return 0;
}

static void jack_shutdown(void *arg) {
  exit(1);
}

int main(int argc, char **argv) {

  /* parse arguments */
  const char *client_name = 0;
  const char *server_name = 0;
  jack_options_t options = JackNullOption;
  jack_status_t status;
  while (1) {
    static struct option long_options[] = {
      { "help",    no_argument,       0, 'h' },
      { "client",  required_argument, 0, 'c' },
      { "server",  required_argument, 0, 's' },
      { 0, 0, 0, 0 }
    };
    int option_index = 0;
    int c = getopt_long(argc, argv, "hc:s:", long_options, &option_index);
    if (c == -1) {
      break;
    }
    switch (c) {
      case 'h':
        fprintf(stderr,
          "Usage: %s [options]\n"
          "Optional:\n"
          "  --client  -c <str>  JACK client name\n"
          "  --server  -s <str>  JACK server name\n"
          "Information:\n"
          "  --help    -h        display this information\n"
	  , argv[0]
        ); return 0; break;
      case 'c': client_name = optarg; break;
      case 's': server_name = optarg; break;
      case '?': break;
      default: exit(1); break;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "excess arguments, try --help\n");
    return 1;
  }
  if (!client_name) {
    client_name = strrchr(argv[0], '/');
    if (!client_name) {
      client_name = argv[0];
    } else {
      client_name++;
    }
  }
  if (server_name) {
    options = (jack_options_t) (options | JackServerName);
  }

  /* connect to JACK */
  client = jack_client_open(client_name, options, &status, server_name);
  if (!client) {
    fprintf(stderr, "jack_client_open() failed, status = 0x%2.0x\n", status);
    if (status & JackServerFailed) {
      fprintf(stderr, "Unable to connect to JACK server\n");
    }
    return 1;
  }
  if (status & JackServerStarted) {
    fprintf(stderr, "JACK server started\n");
  }
  if (status & JackNameNotUnique) {
    client_name = jack_get_client_name(client);
    fprintf(stderr, "unique name `%s' assigned\n", client_name);
  }

  /* initialize */
  state state;
  init(&state);
  jack_set_process_callback(client, process, &state);
  jack_on_shutdown(client, jack_shutdown, 0);

  // printf ("engine sample rate: %d\n", (int) jack_get_sample_rate (client));

  /* create ports */
  input_port [0] = jack_port_register(client, "input_1",  JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput,  0);
  input_port [1] = jack_port_register(client, "input_2",  JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput,  0);
  output_port[0] = jack_port_register(client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  output_port[1] = jack_port_register(client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);

  /* check ports */
  if (!input_port[0] || !input_port[1] || !output_port[0] || !output_port[1]) {
    fprintf(stderr, "no more JACK ports available\n");
    return 1;
  }

  /* activate */
  if (jack_activate(client)) {
    fprintf(stderr, "cannot activate client");
    return 1;
  }

  /* connect ports */
  jack_connect(client, "system:capture_1", jack_port_name(input_port[0]));
  jack_connect(client, "system:capture_1", jack_port_name(input_port[1]));
  jack_connect(client, jack_port_name(output_port[0]), "system:playback_1");
  jack_connect(client, jack_port_name(output_port[1]), "system:playback_2");

  /* process callbacks */
  while (1) {
    sleep(1);
  }

  /* finish */
  jack_client_close(client);
  return 0;
}
