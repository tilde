#include <algorithm>
#include <cmath>

// typedef double sample;
struct sample {
  double x;

  inline sample() { }

  inline sample(sample const &y) : x(y.x) { if (!std::isfinite(x)) { x = 0; } }
  inline sample(double const &y) : x(y) { if (!std::isfinite(x)) { x = 0; } }
  inline sample(float const &y) : x(y) { if (!std::isfinite(x)) { x = 0; } }
  inline sample(int const &y) : x(y) { }

  explicit operator int() { return x; }

  inline sample& operator=(sample const &y) { x = y.x; return *this; }

  inline sample& operator+=(sample const &y) { x += y.x; return *this; }
  inline sample& operator-=(sample const &y) { x -= y.x; return *this; }
  inline sample& operator*=(sample const &y) { x *= y.x; return *this; }
  inline sample& operator/=(sample const &y) { if (y.x != 0) { x /= y.x; } else { x = 0; } return *this; }

};

inline sample operator+(sample const &l, sample const &r) { return l.x + r.x; }
inline sample operator-(sample const &l, sample const &r) { return l.x - r.x; }
inline sample operator*(sample const &l, sample const &r) { return l.x * r.x; }
inline sample operator/(sample const &l, sample const &r) { return l.x / r.x; }

inline sample operator-(sample const &l) { return -l.x; }

inline sample operator%(sample const &l, sample const &r) { int y = r.x; if (y != 0) { return int(l.x) % y; } else { return 0; } }

inline sample operator!(sample const &l) { return !l.x; }
inline sample operator&&(sample const &l, sample const &r) { return l.x && r.x; }
inline sample operator||(sample const &l, sample const &r) { return l.x || r.x; }

inline bool operator< (sample const &l, sample const &r) { return l.x <  r.x; }
inline bool operator<=(sample const &l, sample const &r) { return l.x <= r.x; }
inline bool operator!=(sample const &l, sample const &r) { return l.x != r.x; }
inline bool operator==(sample const &l, sample const &r) { return l.x == r.x; }
inline bool operator> (sample const &l, sample const &r) { return l.x >  r.x; }
inline bool operator>=(sample const &l, sample const &r) { return l.x >= r.x; }

inline sample abs(sample const &l) { return std::abs(l.x); }
inline sample floor(sample const &l) { return std::floor(l.x); }
inline sample fmod(sample const &l, sample const &r) { return std::fmod(l.x, r.x); }
inline sample sqrt(sample const &l) { return std::sqrt(l.x); }

inline sample exp(sample const &l) { return std::exp(l.x); }
inline sample pow10(sample const &l) { return std::pow(10, l.x); }
inline sample pow(sample const &l, sample const &r) { return std::pow(l.x, r.x); }

inline sample log(sample const &l) { return std::log(l.x); }
inline sample log10(sample const &l) { return std::log10(l.x); }

inline sample sin(sample const &l) { return std::sin(l.x); }
inline sample cos(sample const &l) { return std::cos(l.x); }

inline sample tanh(sample const &l) { return std::tanh(l.x); }

static inline sample clip(sample x, sample lo, sample hi) {
  return std::min(std::max(x, lo), hi);
}

static inline int clip(int x, int lo, int hi) {
  return std::min(std::max(x, lo), hi);
}

static inline sample wrap(sample x) {
  return x - floor(x);
}

static inline sample rmstodb(sample x) {
  if (x <= 0) {
    return 0;
  } else {
    return std::max(sample(0), 100 + 20 * log10(x));
  }
}

static inline sample dbtorms(sample x) {
  if (x <= 0) {
    return 0;
  } else {
    return pow10((std::min(x, sample(485)) - 100) / 20);
  }
}

static inline sample mtof(sample x) {
  if (x <= -1500) {
    return 0;
  } else {
    return 8.17579891564 * exp(0.0577622650 * std::min(x, sample(1499)));
  }
}

static inline sample interpolate4(sample const &x, sample const &a, sample const &b, sample const &c, sample const &d) {
  sample a1 = 0.5 * (c - a);
  sample a2 = a - 2.5 * b + 2 * c - 0.5 * d;
  sample a3 = 0.5 * (d - a) + 1.5 * (b - c);
	return ((a3 * x + a2) * x + a1) * x + b;
}

static inline sample eif(sample const &b, sample const &t, sample const &f) {
  return b != 0 ? t : f;
}

static inline sample efloor(sample const &x, int const &y) {
  return floor(x);
}

static inline sample efloor(sample const &x) {
  return floor(x);
}

static inline sample erandom(sample const &l, sample const &r) {
  int i1 = l.x;
  int i2 = r.x;
  return i1 + (((i2 - i1) * (rand() & 0x7fff)) >> 15);
}
