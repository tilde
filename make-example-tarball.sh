#!/bin/bash
set -e
z="$(pwd)"
h="$(dirname "$(readlink -f "${0}")")"
o="GOSUB10-004-src-0xA--expr_"
e="tilde-examples-0.1"
t="$(mktemp -d)"
cd "${t}"
mkdir "${e}"
wget "https://www.archive.org/download/GOSUB10-004/${o}.tgz"
tar xzf "${o}.tgz"
patch -d "${o}" < "${h}/${o}.diff"
cd "${h}"
for p in "${t}/${o}"/*.pd
do
  cp -avit "${t}/${e}/" "${p}"
  cabal run -- tilde "${t}/${e}/$(basename "${p}")"
done
cp -avit "${t}/${e}/" "puredata.h" "puredata.c" "sample.h"
cp -avi  "Makefile.examples" "${t}/${e}/Makefile"
cp -avi  "README.examples"   "${t}/${e}/README.tilde"
cp -avit "${t}/${e}/" "${t}/${o}/COPYING" "${t}/${o}/README"
mkdir -p dist
cd "${t}"
tar --create --verbose --owner=0 --group=0 --gzip --file="${h}/dist/${e}.tgz" "${e}/"
cd "${z}"
