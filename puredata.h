#include <stdlib.h>

#include "sample.h"

#define PI sample(3.141592653589793)
#define SR sample(48000.0)

#define SIGf_state(S,R) sample S;
#define SIGf_init(S,R) S = R;
#define SIGf_dsp(S,R,O) O = S;

#define SIG_state(S) SIGf_state(S,0)
#define SIG_init(S) SIGf_init(S,0)
#define SIG_dsp(S,O) SIGf_dsp(S,0,O)

#define ADDf_state(S,R) sample S;
#define ADDf_init(S,R) S = R;
#define ADDf_dsp(S,R,O,L) O = L + S;

#define SUBf_state(S,R) sample S;
#define SUBf_init(S,R) S = R;
#define SUBf_dsp(S,R,O,L) O = L - S;

#define MULf_state(S,R) sample S;
#define MULf_init(S,R) S = R;
#define MULf_dsp(S,R,O,L) O = L * S;

#define DIVf_state(S,R) sample S;
#define DIVf_init(S,R) S = R;
#define DIVf_dsp(S,R,O,L) O = L / S;

#define ADD_dsp(S,O,L,R) O = L + R;
#define SUB_dsp(S,O,L,R) O = L - R;
#define MUL_dsp(S,O,L,R) O = L * R;
#define DIV_dsp(S,O,L,R) O = L / R;

#define DBTORMS_dsp(S,O,I) O = dbtorms(I);
#define RMSTODB_dsp(S,O,I) O = rmstodb(I);
#define SQRT_dsp(S,O,I) O = sqrt(I);
#define TANH_dsp(S,O,I) O = tanh(I);
#define COS_dsp(S,O,I) O = cos(I * 2 * PI);
#define WRAP_dsp(S,O,I) O = wrap(I);
#define ABS_dsp(S,O,I) O = abs(I);
#define MTOF_dsp(S,O,I) O = mtof(I);

#define HIPf_state(S,HZ) struct { sample last, coef; } S;
#define HIPf_init(S,HZ) { S.last = 0; S.coef = clip(1 - HZ * 2 * PI / SR, 0.0, 1.0); }
#define HIPf_dsp(S,HZ,O,I) { sample next = I + S.coef * S.last; O = next - S.last; S.last = next; }

#define HIP_state(S) HIPf_state(S,0)
#define HIP_init(S) HIPf_init(S,0)
#define HIP_dsp(S,O,I) HIPf_dsp(S,0,O,I)

#define LOPf_state(S,HZ) struct { sample last, coef, feedback; } S;
#define LOPf_init(S,HZ) { S.last = 0; S.coef = clip(HZ * 2 * PI / SR, 0.0, 1.0); S.feedback = 1 - S.coef; }
#define LOPf_dsp(S,HZ,O,I) { O = (S.last = S.coef * I + S.feedback * S.last); }

#define LOP_state(S) LOPf_state(S,0)
#define LOP_init(S) LOPf_init(S,0)
#define LOP_dsp(S,O,I) LOPf_dsp(S,0,O,I)

#define PHASORf_state(S,HZ) struct { sample last, incr; } S;
#define PHASORf_init(S,HZ) { S.last = 0; S.incr = HZ / SR; }
#define PHASORf_dsp(S,HZ,O) { O = (S.last = wrap(S.last + S.incr)); }

#define PHASOR_state(S) struct { sample last; } S;
#define PHASOR_init(S) { S.last = 0; }
#define PHASOR_dsp(S,O,I) { O = (S.last = wrap(S.last + I / SR)); }

#define OSCf_state(S,HZ) struct { sample last, incr; } S;
#define OSCf_init(S,HZ) { S.last = 0; S.incr = HZ / SR; }
#define OSCf_dsp(S,HZ,O) { S.last = wrap(S.last + S.incr); O = cos(2 * PI * S.last); }

#define OSC_state(S) struct { sample last; } S;
#define OSC_init(S) { S.last = 0; }
#define OSC_dsp(S,O,I) { S.last = wrap(S.last + I / SR); O = cos(2 * PI * S.last); }

static int NOISE_seed = 307;
#define NOISE_state(S) int S;
#define NOISE_init(S) S = (NOISE_seed *= 1319);
#define NOISE_dsp(S,O) { O = sample((S & 0x7fffffff) - 0x40000000) * sample(1.0 / 0x40000000); S = S * 435898247 + 382842987; }

#define SAMPHOLD_state(S) struct { sample hold, ctrl; } S;
#define SAMPHOLD_init(S) { S.hold = 0; S.ctrl = 0; }
#define SAMPHOLD_dsp(S,O,L,R) { if (R < S.ctrl) { S.hold = L; } S.ctrl = R; O = S.hold; }

#define TABLE_state(S,NAME,SIZE) struct { int length; sample *buffer; } table_##NAME;
#define TABLE_init(S,NAME,SIZE) { state->table_##NAME.length = SIZE; state->table_##NAME.buffer = (sample *) calloc(state->table_##NAME.length, sizeof(sample)); }

#define TABOSC4f_state(S,NAME,HZ) struct { sample last, incr; } S;
#define TABOSC4f_init(S,NAME,HZ) { S.last = 0; S.incr = HZ / SR; }
#define TABOSC4f_dsp(S,NAME,HZ,O) { \
  int L = state->table_##NAME.length; \
  sample *B = state->table_##NAME.buffer; \
  sample rindex = S.last * (L - 3) + 1; \
  sample f = wrap(rindex); \
  int ri = clip(int(floor(rindex), 1, L - 3)); \
  int i0 = ri - 1; \
  int i1 = ri    ; \
  int i2 = ri + 1; \
  int i3 = ri + 2; \
  sample s0 = B[i0]; \
  sample s1 = B[i1]; \
  sample s2 = B[i2]; \
  sample s3 = B[i3]; \
  O = interpolate4(f, s0, s1, s2, s3); \
  S.last = wrap(S.last + S.incr); \
}

#define TABOSC4_state(S,NAME) struct { sample last; } S;
#define TABOSC4_init(S,NAME) { S.last = 0; }
#define TABOSC4_dsp(S,NAME,O,I) { \
  int L = state->table_##NAME.length; \
  sample *B = state->table_##NAME.buffer; \
  sample rindex = S.last * (L - 3) + 1; \
  sample f = wrap(rindex); \
  int ri = clip(int(floor(rindex), 1, L - 3)); \
  int i0 = ri - 1; \
  int i1 = ri    ; \
  int i2 = ri + 1; \
  int i3 = ri + 2; \
  sample s0 = B[i0]; \
  sample s1 = B[i1]; \
  sample s2 = B[i2]; \
  sample s3 = B[i3]; \
  O = interpolate4(f, s0, s1, s2, s3); \
  S.last = wrap(S.last + I / SR); \
}

#define TABWRITE_state(S,NAME) struct { int windex, writing; } S;
#define TABWRITE_init(S,NAME) { S.windex = 0; S.writing = 0; }
#define TABWRITE_dsp(S,NAME,I) { \
  if (S.writing) { \
    if (S.windex < state->table_##NAME.length) { \
      state->table_##NAME.buffer[S.windex++] = I; \
    } else { \
      S.writing = 0; \
    } \
  } \
}
#define TABWRITE_0_bang(S,NAME) { S.windex = 0; S.writing = 1; }

#define DELWRITE_state(S,NAME,MS) struct { int length, windex; sample *buffer; } delwrite_##NAME;
#define DELWRITE_init(S,NAME,MS) { state->delwrite_##NAME.length = int(MS * SR / 1000); state->delwrite_##NAME.windex = 0; state->delwrite_##NAME.buffer = (sample *) calloc(state->delwrite_##NAME.length, sizeof(sample)); }
#define DELWRITE_dsp(S,NAME,MS,I) { state->delwrite_##NAME.buffer[state->delwrite_##NAME.windex] = I; state->delwrite_##NAME.windex += 1; state->delwrite_##NAME.windex %= state->delwrite_##NAME.length; }

#define DELREAD_state(S,NAME,MS) sample S;
#define DELREAD_init(S,NAME,MS) { S = MS; }
#define DELREAD_dsp(S,NAME,MS,O) { \
  int L = state->delwrite_##NAME.length; \
  int W = state->delwrite_##NAME.windex + L; \
  sample *B = state->delwrite_##NAME.buffer; \
  sample rindex = S * SR / 1000; \
  sample f = wrap(rindex); \
  int ri = clip(int(floor(rindex)), 1, L - 4); \
  int i0 = (W - (ri - 1)) % L; \
  int i1 = (W - (ri    )) % L; \
  int i2 = (W - (ri + 1)) % L; \
  int i3 = (W - (ri + 2)) % L; \
  sample s0 = B[i0]; \
  sample s1 = B[i1]; \
  sample s2 = B[i2]; \
  sample s3 = B[i3]; \
  O = interpolate4(f, s0, s1, s2, s3); \
}

#define VD_state(S,NAME)
#define VD_init(S,NAME)
#define VD_dsp(S,NAME,O,I) { \
  int L = state->delwrite_##NAME.length; \
  int W = state->delwrite_##NAME.windex + L; \
  sample *B = state->delwrite_##NAME.buffer; \
  sample rindex = I * SR / 1000; \
  sample f = wrap(rindex); \
  int ri = clip(int(floor(rindex).x), 1, L - 4); \
  int i0 = (W - (ri - 1)) % L; \
  int i1 = (W - (ri    )) % L; \
  int i2 = (W - (ri + 1)) % L; \
  int i3 = (W - (ri + 2)) % L; \
  sample s0 = B[i0]; \
  sample s1 = B[i1]; \
  sample s2 = B[i2]; \
  sample s3 = B[i3]; \
  O = interpolate4(f, s0, s1, s2, s3); \
}

#define SEND_state(S,NAME) sample send_##NAME;
#define SEND_init(S,NAME) { state->send_##NAME = 0; }
#define SEND_dsp(S,NAME,I) { state->send_##NAME = I; }

#define RECEIVE_state(S,NAME)
#define RECEIVE_init(S,NAME)
#define RECEIVE_dsp(S,NAME,O) { O = state->send_##NAME; }

#define ADC_state(S) sample *S[2];
#define ADC_init(S) { S[0] = &(state->adc[0]); S[1] = &(state->adc[1]); }
#define ADC_dsp(S,L,R) { L = *(S[0]); R = *(S[1]); }

#define DAC_state(S) sample *S[2];
#define DAC_init(S) { S[0] = &(state->dac[0]); S[1] = &(state->dac[1]); }
#define DAC_dsp(S,L,R) { *(S[0]) += L; *(S[1]) += R; }
