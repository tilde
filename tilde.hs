{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Main (main) where

import Data.List(foldl', intercalate, sort, genericLength)
import Safe (atMay)
import System.Environment(getArgs)
import System.FilePath (replaceExtension)
import Data.Maybe (isJust)
import Data.Monoid (mconcat)
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Graph.Inductive as G
import Data.Graph.Inductive.PatriciaTree (Gr)

import Debug.Trace (trace)

type Node = G.LNode Object
type Edge = G.LEdge Wire
type Graph = (String, Gr Object Wire, ([ObjectID], [ObjectID]))

data Object = Object String | Graph Graph
type Wire = (OutletID, InletID)

type Path = [ObjectID]

type WireName = String

type Code = (String, String, String)

state :: Path -> String
state = intercalate "_" . ("state":) . map (show . unObj)

state' :: Path -> String
state' = ("state->" ++) . state

inlet :: Path -> InletID -> WireName
inlet p i = intercalate "_" . ("wire":) . (++["i", show . unIn $ i]) . map (show . unObj) $ p

outlet :: Path -> OutletID -> WireName
outlet p o = intercalate "_" . ("wire":) . (++["o", show . unOut $ o]) . map (show . unObj) $ p

graph :: PdPatch -> Graph
graph pd = if not . S.null $ S.unions[S.fromList[f,t]|(f,t,_)<-es] `S.difference` S.fromList (map fst ns) then error . show $ (map fst ns, es) else
  (name pd, G.mkGraph ns es, (map snd . sort . inlets $ pd, map snd . sort . outlets $ pd))
  where
    ns = zipWith node [0..] . objects $ pd
    es = map edge . connects $ pd


node :: G.Node -> PdObject -> Node
node n (PdObject s) = (n, Object s)
node n (PdSubpatch pd) = (n, Graph . graph $ pd)

edge :: Connect -> Edge
edge (fobj, fout, tobj, tin) = (fromIntegral fobj, fromIntegral tobj, (fout, tin))

dspGraph :: KnownObjects -> Gr Object Wire -> Gr Object Wire
dspGraph objs g = G.mkGraph (filter (dspNode objs g) (G.labNodes g)) (filter (dspEdge objs g) (G.labEdges g))

dspNode :: KnownObjects -> Gr Object Wire -> Node -> Bool
dspNode objs g (n, Object s) = if any (dspOutlet objs g)
  (  [ (m, o) | (m, (o, _)) <- G.lpre g n ]
  ++ [ (n, o) | (_, (o, _)) <- G.lsuc g n ] ) then True else trace ("warning: discarded: " ++ s) False
dspNode _ _ (_, Graph _) = True

dspEdge :: KnownObjects -> Gr Object Wire -> Edge -> Bool
dspEdge objs g (m, n, (o, i))
  | dspOutlet objs g (m, o) && dspInlet objs g (n, i) = True
  | dspOutlet objs g (m, o) = trace "warning: signal -> non-signal" False
  | dspInlet objs g (n, i) = trace "warning: non-signal -> signal" False
  | otherwise = False

dspOutlet objs g (n, o) = case G.lab g (fromIntegral n) of
  Just (Object s) -> case findObject objs s of
    Just (_, _, os) -> case os `atMay` fromIntegral o of
      Just SignalIO -> True
      Just _ -> False
    Nothing -> False
  Just (Graph (_, g', (_, os))) -> case os `atMay` fromIntegral o of
    Just m -> case G.lab g' (fromIntegral m) of
      Just (Object s) -> case words s of
        ("outlet~":_) -> True
        ("outlet":_) -> False

dspInlet objs g (n, i) = case G.lab g (fromIntegral n) of
  Just (Object s) -> case findObject objs s of
    Just (_, is, _) -> case is `atMay` fromIntegral i of
      Just SignalIO -> True
      Just _ -> False
    Nothing -> False
  Just (Graph (_, g', (is, _))) -> case is `atMay` fromIntegral i of
    Just m -> case G.lab g' (fromIntegral m) of
      Just (Object s) -> case words s of
        ("inlet~":_) -> True
        ("inlet":_) -> False

isDSPInlet g n = case G.lab g n of
  Just (Object s) -> case words s of
    ("inlet~":_) -> True
    ("inlet":_) -> False

isDSPOutlet g n = case G.lab g n of
  Just (Object s) -> case words s of
    ("outlet~":_) -> True
    ("outlet":_) -> False

compileDSP :: KnownObjects -> Path -> Graph -> Code
compileDSP objs path (sn, g, (_, _)) = mconcat . map mconcat $
  [ [ ("","","const sample " ++ inlet path' n_in ++ " = " ++ (intercalate " + " . ("0":)) [ outlet (path ++ [fromIntegral o]) o_out | (o, o_out) <- from ] ++ ";\n")
    | (n_in, from) <- M.toAscList froms
    ] ++
    [ ("","","sample " ++ outlet path' n_out ++ ";\n") | n_out <- outs ] ++
    ( case n_name of
      Object s ->
        [ ("/* [" ++ s ++ "] */\n", "/* [" ++ s ++ "] */\n", "/* [" ++ s ++ "] */\n")
        , compileDSPObject objs path' s
            (M.fromList[(n_in, inlet path' n_in) | n_in <- M.keys froms])
            (M.fromList[(n_out, outlet path' n_out) | n_out <- outs])
        ]
      Graph (sn', g', (is', os')) ->
        [ ("", "", "{ /* subpatch " ++ sn' ++ " */\n" ++ concat [ "const sample inlet_" ++ show (unObj k) ++ " = " ++ inlet path' n_in ++ ";\n" | (k, n_in) <- is'`zip`[0..], isDSPInlet g' (fromIntegral k) ])
        , compileDSP objs path' (sn', g', (is', os'))
        , ("", "", concat [ outlet path' n_out ++ " = outlet_" ++ show (unObj k) ++ ";\n" | (k, n_out) <- os'`zip`[0..], isDSPOutlet g' (fromIntegral k) ] ++ "}\n")
        ]
    )
  | let gd = dspGraph objs g
  , n <- G.topsort gd
  , let path' = path ++ [fromIntegral n]
  , let Just n_name = G.lab gd n
  , let froms = M.fromListWith (++) [ (n_in, [(o, o_out)]) | (o, (o_out, n_in)) <- G.lpre gd n ]
  , let outs = (S.toList . S.fromList) [ n_out | (_, (n_out, _)) <- G.lsuc gd n ]
  ]

compileDSPObject :: KnownObjects -> Path -> String -> M.Map InletID String -> M.Map OutletID String -> Code
compileDSPObject objs path s is os
  | obj == "inlet~" = ("", "", case 0 `M.lookup` os of -- FIXME
        Nothing -> "/* inlet not found in outlet list */\n"
        Just o -> o ++ " = inlet_" ++ show (unObj $ last path) ++ ";\n"
      )
--  | obj == "outlet"  = ("", "", "sample outlet_" ++ show (last path) ++ " = " ++ maybe "0" id (0 `M.lookup` is) ++ ";\n") -- FIXME
  | obj == "outlet~" = ("", "", "const sample outlet_" ++ show (unObj $ last path) ++ " = " ++ maybe "0" id (0 `M.lookup` is) ++ ";\n")
  | obj == "expr~" =
      let outs = map (`M.lookup` os) [ 0 .. 9 ]
          es = map (filter (/= '$')) . unsemi . filter (/= '\\') . unwords $ args
      in  ( ""
          , ""
          , "{\n" ++
            concat [ "const sample v" ++ show (unIn (k + 1)) ++ " = " ++ maybe "0" id (k `M.lookup` is) ++ ";\n" | k <- [ 0 .. 9 ] ] ++
            "#define if(b,t,f) eif(b,t,f)\n" ++
            "#define floor(...) efloor(__VA_ARGS__)\n" ++
            "#define random(x,y) erandom(x,y)\n" ++
            concat [ o ++ " = " ++ e ++ ";\n" | (mo, e) <- outs `zip` es, isJust mo, let Just o = mo ] ++
            "#undef random\n" ++
            "#undef floor\n" ++
            "#undef if\n" ++
            "}\n"
          )
  | otherwise = case findObject objs s of
      Nothing -> ("/* not supported */\n", "/* not supported */\n", "/* not supported */\n")
      Just (o, ni, no) ->
        let ins  = map (maybe "0" id . (`M.lookup` is)) [ k | (k, t) <- [0..]`zip`ni, t == SignalIO ]
            outs = map (maybe "disconnected" id . (`M.lookup` os)) [ k | (k, t) <- [0..]`zip`no, t == SignalIO ]
        in  ( "#ifdef " ++ o ++ "_state\n" ++ o ++ "_state(" ++ intercalate "," (state path : args) ++ ")\n#endif\n"
            , "#ifdef " ++ o ++ "_init\n" ++ o ++ "_init(" ++ intercalate "," (state' path : args) ++ ")\n#endif\n"
            , o ++ "_dsp(" ++ intercalate "," ([state' path] ++ args ++ outs ++ ins) ++ ")\n"
            )
  where
    obj = words s !! 0
    args = drop 1 (words s)

inspect :: String -> (String, [ArgT])
inspect s = (words s !! 0, map checkType . drop 1 . words $ s)

findObject :: KnownObjects -> String -> Maybe (String, [IoletT], [IoletT])
findObject objs s = findObject' args0
  where
    (o, args0) = inspect s
    findObject' as = case (o, as) `M.lookup` objs of
      Nothing -> case (o, as ++ [GimmeArg]) `M.lookup` objs of
        Nothing -> case as of
          [] -> Nothing
          _ -> findObject' (init as)
        r -> r
      r -> r

unsemi :: String -> [String]
unsemi xs = let (ys, zs) = span (/= ';') xs
            in  ys : case zs of
              [] -> []
              (';':zs') -> unsemi zs'
              _ -> error "unsemi"

checkType :: String -> ArgT
checkType s = case (reads :: ReadS Float) s of
  [(_,"")] -> FloatArg
  _ -> SymbolArg

flatten :: Code -> String
flatten (s, i, d) = "#include \"puredata.h\"\ntypedef struct {\nsample adc[2];\nsample dac[2];\n" ++ s ++ "} state;\n" ++ "static inline void init(state *state) {\n" ++ i ++ "}\n" ++ "static inline void dsp(state *state) {\nsample disconnected;\n" ++ d ++ "}\n#include \"puredata.c\"\n"

data IoletT = SignalIO | FloatIO deriving (Eq, Ord, Show)
data ArgT = FloatArg | SymbolArg | GimmeArg deriving (Eq, Ord)

parseArgT :: String -> [ArgT]
parseArgT [] = []
parseArgT "_" = []
parseArgT "*" = [GimmeArg]
parseArgT ('f':a) = FloatArg : parseArgT a
parseArgT ('s':a) = SymbolArg : parseArgT a
parseArgT _ = error "parseArgT"

parseIoletT :: String -> [IoletT]
parseIoletT [] = []
parseIoletT "_" = []
parseIoletT ('~':a) = SignalIO : parseIoletT a
parseIoletT ('f':a) = FloatIO : parseIoletT a
parseIoletT _ = error "parseIoletT"

type KnownObjects = M.Map (String, [ArgT]) (String, [IoletT], [IoletT])

parseSPD :: String -> KnownObjects
parseSPD = M.fromList . map ((\[pd,a,c,i,o] -> ((pd, parseArgT a), (c, parseIoletT i, parseIoletT o))) . words) . lines

main' :: KnownObjects -> String -> IO ()
main' objs patch = do
  writeFile (replaceExtension patch ".cc") . flatten . compileDSP objs [] . graph . readPatch patch =<< readFile patch

newtype ObjectID = Obj{unObj:: Int} deriving (Eq, Ord, Enum, Num, Real, Integral, Show)
newtype InletID  = In {unIn :: Int} deriving (Eq, Ord, Enum, Num, Real, Integral, Show)
newtype OutletID = Out{unOut:: Int} deriving (Eq, Ord, Enum, Num, Real, Integral, Show)
type Connect  = (ObjectID, OutletID, ObjectID, InletID)
type InletX   = Int
type OutletX  = Int

data PdObject
  = PdObject   String
  | PdSubpatch PdPatch

data PdPatch = PdPatch {
  name     :: String,
  objects  :: [PdObject],
  connects :: [Connect],
  inlets   :: [(InletX, ObjectID)],
  outlets  :: [(OutletX, ObjectID)]
}

readPatch :: String -> String -> PdPatch
readPatch s = head . foldl' readLine [emptyPatch s] . tail . joinAgain . lineSemi

lineSemi :: String -> [String]
lineSemi "" = []
lineSemi s  =
  let (l, s') = break (';' ==) . filter ('\n' /=) $ s
  in l : case s' of
    []      -> []
    (_:s'') -> lineSemi s''

joinAgain :: [String] -> [String]
joinAgain [] = []
joinAgain [x] = [x]
joinAgain (x:y:zs)
  | x == []         = joinAgain (y:zs)
  | last x == '\\'  = joinAgain ((init x++";"++y):zs)
  | otherwise       = x : joinAgain (y:zs)

emptyPatch :: String -> PdPatch
emptyPatch s = PdPatch { name = s, objects = [], connects = [], inlets = [], outlets = [] }

readLine :: [PdPatch] -> String -> [PdPatch]
readLine (p:ps) l =
  let atoms = words l
  in case atoms!!0 of
    "#N" -> case atoms!!1 of
      "canvas"    -> let p' = emptyPatch "pd" in
                     p':p:ps
      _ -> error "readLine #N"
    "#X" -> case atoms!!1 of
      "restore"   -> let (p':ps') = ps in
                     p' { objects = objects p' ++ [PdSubpatch $ p { name = unwords (drop 4 atoms) }] } : ps'
      "coords"    -> p : ps
      "obj"       -> case atoms !! 4 of
        "inlet"   -> p { objects = objects p ++ [PdObject $ unwords (drop 4 atoms)], inlets = inlets p ++ [(read $ atoms !! 2, genericLength $ objects p)] } : ps
        "inlet~"  -> p { objects = objects p ++ [PdObject $ unwords (drop 4 atoms)], inlets = inlets p ++ [(read $ atoms !! 2, genericLength $ objects p)] } : ps
        "outlet"  -> p { objects = objects p ++ [PdObject $ unwords (drop 4 atoms)], outlets = outlets p ++ [(read $ atoms !! 2, genericLength $ objects p)] } : ps
        "outlet~" -> p { objects = objects p ++ [PdObject $ unwords (drop 4 atoms)], outlets = outlets p ++ [(read $ atoms !! 2, genericLength $ objects p)] } : ps
        _         -> p { objects = objects p ++ [PdObject $ unwords (drop 4 atoms)] } :  ps
      "msg"       -> p { objects = objects p ++ [PdObject $ unwords ("message" : drop 4 atoms)] } : ps
      "floatatom" -> p { objects = objects p ++ [PdObject $ unwords ("floatatom" : drop 4 atoms)] } : ps
      "text"      -> p { objects = objects p ++ [PdObject $ unwords ("comment" : drop 4 atoms)] } : ps
      "connect"   -> p { connects = connects p ++ [(Obj $ read (atoms!!2), Out $ read (atoms!!3), Obj $ read (atoms!!4), In $ read (atoms!!5))] } : ps
      _ -> error "readLine #X"
    _ -> error "readLine #N/#X"
readLine _ _ = error "readLine"

main :: IO ()
main = do
  objs <- parseSPD `fmap` readFile "puredata.tilde"
  mapM_ (main' objs) =<< getArgs
